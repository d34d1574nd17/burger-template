import React from 'react';
import BuildControl from "./BuildControl/BuildControl";

import "./BuildControls.css";

const controls = [
    {type: "salad"},
    {type: "bacon"},
    {type: "cheese"},
    {type: "meat"}
];

const BuildControls = props => (
        <div className="BuildControls">
            <p>Current Price: <strong>{props.price} KGS</strong></p>
            {controls.map(control => {
                return <BuildControl
                    key={control.type} type={control.type}
                    disabled={props.disabled[control.type]}
                    added={() => props.ingredientAdded(control.type)}
                    removed={() => props.ingredientRemoved(control.type)}
                />
            })}
            <button
                className="OrderButton"
                onClick={props.ordered}
                disabled={!props.purchasable}
            >ORDER NOW</button>
        </div>
    );

export default BuildControls;