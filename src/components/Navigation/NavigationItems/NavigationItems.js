import React from 'react';
import "./NavigationItems.css";
import NavigationItem from "./NavigationItem/NavigationItem";

const NavigationItems = () => {
    return (
        <ul className="NavigationItems">
            <NavigationItem to="/" children="Burger Builder"/>
            <NavigationItem to="/orders" children="Orders"/>
        </ul>
    );
};

export default NavigationItems;