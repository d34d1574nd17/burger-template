import React from 'react';
import "./Logo.css"
import burgerLogo from "../../assets/images/burger_logo.png";

const Logo = props => {
    return (
        <div className="Logo">
            <img src={burgerLogo} alt="My burger"/>
        </div>
    );
};

export default Logo;