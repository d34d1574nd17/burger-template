import React from 'react';

const OrderItem = props => {
    const ingredients = [];

    for ( let ingredientName in props.ingredients) {
        ingredients.push({name: ingredientName, amout: props.ingredients[ingredientName]});
    }
    const ingredientOutput = ingredients.map(ig => (
        <span key={ig.name}>{ig.name} ({ig.amout})</span>
    ));

    return (
        <div className="OrderItem">
            <p>Ingredients: {ingredientOutput}</p>
            <p>Price: <strong>{props.price} KGS</strong></p>
        </div>
    );
};

export default OrderItem;