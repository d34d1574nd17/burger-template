import axios from "axios";

const instance = axios.create({
    baseURL: "https://burger-d764b.firebaseio.com/",
});

export default instance;