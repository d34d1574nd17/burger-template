import React, {Component} from 'react';
import {BrowserRouter, Switch, Route} from "react-router-dom";

import './App.css';
import BurgerBuilder from "./containers/BurgerBuilder/BurgerBuilder";
import Checkout from "./containers/Checkout/Checkout";
import About from "./containers/About/About";
import Layout from "./components/Layout/Layout";
import Orders from "./containers/Orders/Orders";


class App extends Component {
  render() {
    return (
        <Layout>
            <BrowserRouter>
                <Switch>
                    <Route path="/checkout" component={Checkout}/>
                    <Route path="/about" component={About}/>
                    <Route path="/orders" component={Orders}/>
                    <Route path="/" exact component={BurgerBuilder}/>
                </Switch>
            </BrowserRouter>
        </Layout>
    );
  }
}

export default App;
