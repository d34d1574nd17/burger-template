import React, {Component} from 'react';
import axios from '../../axios-orders';
import Spinner from "../../components/UI/Spinner/Spinner";
import OrderItem from "../../components/Order/OrderItem/OrderItem";

class Orders extends Component {
    state = {
      orders: [],
      loading: true
    };

    componentDidMount() {
        axios.get("/orders.json")
            .then(response => {
                const fetchedOrders = [];
                console.log(response.data);
                for (let key in response.data) {
                    fetchedOrders.push({...response.data[key], id: key});
                }
                this.setState({loading: false, orders: fetchedOrders})
            })
            .catch(() => {
                this.setState({loading: false})
            })
    }

    render() {
        let orders = this.state.orders.map(order => (
           <OrderItem key={order.id}
        ingredients={order.ingredients}
        price={order.price}
        />
        ));
        if (this.state.loading) {
            orders = <Spinner/>
        }
        return (
            <div>
                {orders}
            </div>
        );
    }
}

export default Orders;