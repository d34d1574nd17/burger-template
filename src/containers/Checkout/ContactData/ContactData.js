import React, {Component} from 'react';
import Button from "../../../components/UI/Button/Button";
import axios from "../../../axios-orders";

import "./ContactData.css";
import Spinner from "../../../components/UI/Spinner/Spinner";

class ContactData extends Component {
    state = {
        name: "",
        email: "",
        street: "",
        postal: "",
        loading: false
    };

    valueChanged = event => {
        this.setState({[event.target.name]: event.target.value})
    };

    orderHandler = (event) => {
      event.preventDefault();
      this.setState({loading: true});
      const order = {
        ingredients: this.props.ingredients,
        price: this.props.price,
        customer: {
            name: this.state.name,
            email: this.state.email,
            street: this.state.street,
            postal: this.state.postal,
        }
      };

      axios.post("/orders.json", order).finally(() => {
          this.setState({loading: false});
          this.props.history.push("/");
      })
    };

    render() {
        let form = (
            <form >
                <input type="text" required
                       className="Input" name="name"
                       placeholder="Your name"
                       onChange={this.valueChanged}
                       value={this.state.name}
                />
                <input type="email" required
                       className="Input" name="email"
                       placeholder="Your Mail"
                       onChange={this.valueChanged}
                       value={this.state.email}
                />
                <input type="text" required
                       className="Input" name="street"
                       placeholder="Street"
                       onChange={this.valueChanged}
                       value={this.state.street}
                />
                <input type="text" required
                       className="Input" name="postal"
                       placeholder="Postal Code"
                       onChange={this.valueChanged}
                       value={this.state.postal}
                />
                <Button btnType="Success" clicked={(event) => this.orderHandler(event)}>ORDER</Button>
            </form>
        );

        if(this.state.loading) {
            form = <Spinner/>
        }
        return (
            <div className="ContactData">
                <h4>Enter your Contact Data</h4>
                {form}
            </div>
        );
    }
}

export default ContactData;